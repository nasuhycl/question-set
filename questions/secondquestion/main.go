package main

import "fmt"

func main() {

	RecursiveFunction(9)

}

// * Default pattern is 2,2,3
var firstNumber = 2
var secondNumber = 2
var thirdNumber = 3

// *Recursive function 2,2,3 pattern with count parameter to stop the function when the count is reached.
func RecursiveFunction(count int) {
	if firstNumber > count {
	} else {
		fmt.Println(firstNumber)
		firstNumber = firstNumber + secondNumber
		secondNumber = secondNumber + thirdNumber
		thirdNumber = thirdNumber + 3
		RecursiveFunction(count)
	}
}
