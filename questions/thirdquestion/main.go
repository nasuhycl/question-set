package main

import "fmt"

func main() {

	//All strings in the list.
	inputs := []string{"apple", "pie", "apple", "red", "red", "red"}

	//Collect words and repetitions in a list.
	dataList := make(map[string]int)
	for _, str := range inputs {
		dataList[str]++
	}
	//We search the list to find the word with the highest number.
	val := 0
	str := ""
	for k, v := range dataList {
		if v > val {
			val = v
			str = k
		}
	}
	fmt.Printf(str)
}
