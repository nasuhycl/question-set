package main

import (
	"fmt"
	"sort"
	"strings"
)

type ByAAndLength []string

//* Len is the number of elements in the collection.
func (s ByAAndLength) Len() int {
	return len(s)
}

//* Swap swaps the elements with indexes i and j.
func (s ByAAndLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s ByAAndLength) Less(i, j int) bool {
	// Compare the number of 'a's in each word (in descending order)
	aCountI := strings.Count(s[i], "a")
	aCountJ := strings.Count(s[j], "a")
	if aCountI != aCountJ {
		return aCountI > aCountJ
	}

	// For words with the same number of 'a's, compare their lengths (in descending order)
	return len(s[i]) > len(s[j])
}

//* Sorts the given slice of strings by the number of 'a's in each word (in descending order) and then by the length of each word (in descending order)
func sortWords(words []string) []string {
	sort.Sort(ByAAndLength(words))
	return words
}

func main() {
	words := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}

	sortedWords := sortWords(words)
	fmt.Println(sortedWords)
	// Output: ["aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"]
}
